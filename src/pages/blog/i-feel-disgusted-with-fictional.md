---
title: I feel disgusted with fictional content
date: 2021-11-26
author: Miami Autumn
description: In the course of exploring your sexuality, you may have stumbled across fictional sexual media of childlike characters and felt negatively about yourself or about this behaviour.
layout: ../../layouts/PostLayout.astro
---

# I feel disgusted with myself after viewing fictional media of childlike characters having sex

In the course of exploring your sexuality, you may have stumbled across fictional sexual media of childlike characters and felt negatively about yourself or about this behaviour. These feelings can be understandable. Exploring sexuality can be exciting, but it can also sometimes be frightening to experience new things and find out more about yourself.

Feeling attracted to something or someone you didn't anticipate can bring feelings of unexpectedness uneasiness, as can seeing sexual media you're uninterested in. It's important to remember that no matter what you are or aren't aroused by, your feelings are valid.

This material—sexual media depicting characters with features commonly associated with children and adolescents (eg, short stature, flat chests, playful personalities, etc)—is typically called lolicon (feminine characters), shotacon (masculine characters) or toddlercon (very young-appearing characters).

## Are these media harmful?

This material does not involve real humans other than the artist and cannot be seen as abusive in nature. But, despite this, fictional sexual media are criminalised to various extents in Australia, Canada (exempt if self-produced), the UK and the US (legal grey area).

Many people who are uncomfortable with viewing sexual media of children or adolescents (SMCA) or who are worried about legal ramifications find satisfaction in viewing fictional sexual media. Fictional sexual media have even been suggested by researchers as an alternative for SMCA (https://doi.org/10.1007/s10508-010-9696-y).

There are also situations where viewing fictional sexual media can be personally problematic. For some, viewing fictional sexual media can lead to stress and paranoia because of the fear of potential legal ramifications or feelings of guilt because of a belief that fictional sexual media are in some way abusive in nature. If viewing these media is causing you to be distressed or is interfering with your daily responsibilities, then it is possible it is problematic for you. This doesn't necessarily mean you need to stop viewing it (although, this may be an option if you believe it's right for you), but it could benefit you to limit the amount of time spend in these activities.
