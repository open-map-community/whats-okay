---
title: Wondering about my sexual thoughts about children
date: 2021-11-26
author: Miami Autumn
description: Having questions about sexuality is natural and healthy.
layout: ../../layouts/PostLayout.astro
---

# Wondering about my sexual thoughts about children

Having questions about sexuality is natural and healthy. People of all ages experience sexuality, and sexuality can naturally fluctuate as we grow and develop. No matter what your sexuality is, even if you are attracted to children, your feelings are valid, and you deserve respect and acceptance.

## What is minor attraction?

Minor attraction is a normal, common variation in human sexuality. Attraction to minors (younger than 15 years old) is common among people of all ages, genders and backgrounds. While the cause of minor attraction is unknown, it is widely thought to be primarily biological, meaning it is out of anyone's control.

Minor attraction can be directed at children and adolescents of any genders and of any ages from newborns to 15 year olds. Minor-attracted persons (MAPs) attracted primarily to infants and toddlers (approximately ages 0-4) are known as 'nepiophiles' while MAPs attracted primarily to prepubescents (approximately ages 3-11) are known as 'paedophiles' and MAPs attracted primarily to adolescents (approximately ages 10-14) are known as 'hebephiles'.

Attraction to minors can be either exclusive, meaning the MAP is only attracted to minors, or non-exclusive, meaning the MAP also has attraction to adults. Some non-exclusive MAPs are attracted predominately or nearly entirely to children while others are attracted predominantely to adults; this depends on the individual.

Estimates suggest somewhere between 1% and 5% of people are attracted to minors (https://psycnet.apa.org/doi/10.1037/11639-000) (https://doi.org/10.1037/14191-000) (https://doi.org/10.1007/s10508-016-0799-y) (https://doi.org/10.1037/0000107-000). Some research studies suggest this number may be as high as 10% or 25% (https://doi.org/10.1111/j.1743-6109.2009.01597.x) (https://www.cybercrimejournal.com/bergenetalijcc2013vol7issue2.pdf).

There is a lot of stigma and ignorant hate directed at MAPs. But, research shows minor attraction is not a mental illness, nor is it inherently harmful. However, MAPs _are_ at an increased risk of mental illness, and nearly 40% of MAPs experience chronic suicidal thoughts (https://doi.org/10.1177%2F1079063219825868). Organisations like [B4U-ACT](https://www.b4uact.org) help to connect MAPs who are struggling with mental illness to MAP-friendly mental health professionals.

## Am I a MAP?

If you are curious about your sexual thoughts about children or are concerned you may be a MAP, consider these questions and your honest answers to them.

- Do you consistently feel a sexual, romantic or other arousal for a children or adolescents (younger than 15 years old) who are at least five years younger than you?
- Do you consistently have sexual, romantic or other fantasies centred around children or adolescents (real or fictional) who are at least five years younger than you?
- Do you sometimes or often masturbate while enjoying media of children or adolescents (real or fictional) who are at least five years younger than you?

If you answer 'yes' to one or more of these questions, it is possible you're a MAP.

## What do I do if I'm a MAP?

Many MAPs live comfortable and happy lives content with their sexuality. However, it is not always an easy road to self-acceptance. Often, MAPs struggle with mental illnesses and have a difficult time accepting their sexuality or living with it.

There are various resources available to MAPs, including peer-support groups and organisations. View our Resources page here on What's Okay for more information about resources available to MAPs.
